class Common {
    static plusOne(n) {
        return n + 1;
    }

    static plusTwo(n) {
        return n + 2;
    }
}

module.exports = Common;