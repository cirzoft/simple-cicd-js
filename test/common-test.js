const Common = require('../common');
const assert = require('assert');

describe('Testing Common Class', () => {
    it('should plus one on input number', () => {
        assert.equal(Common.plusOne(1), 2);
    });
    it('should plus two on input number', () => {
        assert.equal(Common.plusOne(1), 2);
    });
});